

#include <iostream>

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// Other Libs
// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


// Other includes
#include "Shader.h"
#include "Mesh.h"

#include <string>
#include <iostream>

#include "calculations.h"

using namespace std;

// Function prototypes
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
bool keys[1024];
void do_movement();
int getError();
void makeLight(Shader shader);
void collision(Mesh &mesh, GLfloat floor);


// Window dimensions
const GLuint WIDTH = 1024, HEIGHT = 768;

glm::vec3 cameraPos = glm::vec3(-1.5f, 2.0f, 5.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);

GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

GLfloat yaw    = -80.0f;	// Yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right (due to how Eular angles work) so we initially rotate a bit to the left.
GLfloat pitch  =  -20.0f;
GLfloat lastX  =  WIDTH  / 2.0;
GLfloat lastY  =  HEIGHT / 2.0;
GLfloat fov =  45.0f;

bool GPU =true;
float speed=1;

glm::vec3 lightPos(1.2f, 1.0f, 1.0f);

// The MAIN function, from here we start the application and run the game loop
int main(int argc, char **argv)
{
    // Init GLFW
    glfwInit();
    // Set all the required options for GLFW
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    glfwSwapInterval(0);

    // Create a GLFWwindow object that we can use for GLFW's functions
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "LearnOpenGL", nullptr, nullptr);
    glfwMakeContextCurrent(window);

    // Set the required callback functions
    glfwSetKeyCallback(window, key_callback);
    glfwSetScrollCallback(window, scroll_callback);
    glfwSetCursorPosCallback(window, mouse_callback);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);



    // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
    glewExperimental = GL_TRUE;
    // Initialize GLEW to setup the OpenGL Function pointers
    glewInit();

    if(getError() == 1)
        return 0;

    // Define the viewport dimensions
    glViewport(0, 0, WIDTH, HEIGHT);

    // Setup OpenGL options
    glEnable(GL_DEPTH_TEST);


    // Build and compile our shader program
    Shader ourShader("/home/reggis/ClionProjects/gpu-project-jello-cube/shaders/main.vs", "/home/reggis/ClionProjects/gpu-project-jello-cube/shaders/main.fs");

    // Create Vertex Array Object
    GLuint vao, floorVao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    // Create a Vertex Buffer Object and copy the vertex data to it
    GLuint vbo, IBO, vboFloor, IBOFloor;
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &vboFloor);

    GLfloat floorLevel = -3;

    GLfloat floorVertices[] = {
        10,floorLevel,10,0,1,0,0.2,0.5,0.6,
        -10,floorLevel,10,0,1,0,0.2,0.5,0.6,
        10,floorLevel,-10,0,1,0,0.2,0.5,0.6,
        -10,floorLevel,-10,0,1,0,0.2,0.5,0.6,
    };


    GLuint floorIndices[] ={
            0,1,2,
            2,3,1,
    };

    vector<GLuint> indices2= {
            0, 1, 2,
            1, 3, 2,
            4, 5, 6,
            5, 7, 6,
            0, 2, 5,
            2, 7, 5,
            3, 1, 6,
            1, 4, 6,
            1, 0, 4,
            0, 5, 4,
            2, 3, 7,
            3, 6, 7
    };

    int num = 10;
    if(argc >= 2)
    {
        num = atoi(argv[1]);
    }
    cout << "Generating " << num << " size cube" <<endl;
    Mesh mesh(num);

    vector<float> vertices3 = mesh.getVerticles();
    indices2 = mesh.getFaces();


// MESH
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
//
    glBufferData(GL_ARRAY_BUFFER, vertices3.size()*sizeof(float), &vertices3[0], GL_STATIC_DRAW);
//    // Specify the layout of the vertex data

    GLint posAttrib = glGetAttribLocation(ourShader.Program, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), 0);

    GLint normalAttrib = glGetAttribLocation(ourShader.Program, "normal");
    glEnableVertexAttribArray(normalAttrib);
    glVertexAttribPointer(normalAttrib, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (GLvoid*)(3 * sizeof(float)));

    GLint colorAttrib = glGetAttribLocation(ourShader.Program, "color");
    glEnableVertexAttribArray(colorAttrib);
    glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (GLvoid*)(6 * sizeof(float)));

    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glGenBuffers(1, &IBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices2.size()*sizeof(GLuint), &indices2[0], GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);


    if(getError() == 1)
        return 0;
// FLOOR

    glGenVertexArrays(1, &floorVao);
    glBindVertexArray(floorVao);
//
    glBindBuffer(GL_ARRAY_BUFFER, vboFloor);
//
    glGenBuffers(1, &IBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(floorIndices), floorIndices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);

    glBindVertexArray(0);
    if(getError() == 1)
        return 0;
    double lastTime = glfwGetTime();
    int nbFrames = 0;

    float force = 1;
    if(argc >= 3)
    {
        force = atoi(argv[2]);
    }

    // Game loop
    mesh.tickBlock(glm::vec3(0.01*force,0.1*force,0.01*force));

    while (!glfwWindowShouldClose(window))
    {
        double currentTime = glfwGetTime();
        nbFrames++;
        if ( currentTime - lastTime >= 1.0 ){
            printf("%f ms/frame - %f FPS GPU mode: %d\n", 1000.0/double(nbFrames), double(nbFrames), GPU);
            nbFrames = 0;
            lastTime += 1.0;
        }

//        mesh.useForce(glm::vec3(0,-0.001,0));
//        collision(mesh, floorLevel);
//
//        mesh.move();


        mesh.computeForces(GPU, speed);
        mesh.makeMove();
        if(getError() == 1)
            return 0;

        // Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
        glfwPollEvents();
        do_movement();

        // Render
        // Clear the colorbuffer
        glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        ourShader.Use();

        GLint objectColorLoc = glGetUniformLocation(ourShader.Program, "objectColor");
        GLint lightPosLoc = glGetUniformLocation(ourShader.Program, "lightPos");
        GLint viewPosLoc = glGetUniformLocation(ourShader.Program, "viewPos");
        glUniform3f(viewPosLoc, cameraPos.x, cameraPos.y, cameraPos.z);
        glUniform3f(lightPosLoc, lightPos.x, lightPos.y, lightPos.z);
        glUniform3f(objectColorLoc, 1.0f, 0.5f, 0.31f);

        makeLight(ourShader);

        // Create transformations
        glm::mat4 view;
        glm::mat4 projection;
        view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
        projection = glm::perspective(fov, (GLfloat)WIDTH/(GLfloat)HEIGHT, 0.1f, 100.0f);
        // Get their uniform location
        GLint modelLoc = glGetUniformLocation(ourShader.Program, "model");
        GLint viewLoc = glGetUniformLocation(ourShader.Program, "view");
        GLint projLoc = glGetUniformLocation(ourShader.Program, "projection");

        glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
        glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));

        glm::mat4 model;
//        model = glm::scale(model, glm::vec3(0.8f, 0.8f, 0.8f));
        model = glm::translate(model, mesh.getPosition());
        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));


        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        vertices3 = mesh.getVerticles();
        glBufferData(GL_ARRAY_BUFFER, vertices3.size()*sizeof(float), &vertices3[0], GL_STATIC_DRAW);
        glDrawElements(GL_TRIANGLES, indices2.size(), GL_UNSIGNED_INT, 0);

        glm::mat4 modelFloor;

        glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(modelFloor));
        glBindVertexArray(0);

        glBindVertexArray(floorVao);
        glBindBuffer(GL_ARRAY_BUFFER, vboFloor);

        glBufferData(GL_ARRAY_BUFFER, sizeof(floorVertices), floorVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(posAttrib);
        glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (GLvoid*)(0));
        glVertexAttribPointer(normalAttrib, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (GLvoid*)(3 * sizeof(float)));
        glEnableVertexAttribArray(normalAttrib);
        glVertexAttribPointer(colorAttrib, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (GLvoid*)(6 * sizeof(float)));
        glEnableVertexAttribArray(colorAttrib);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

        if(getError() == 1)
            return 0;
        // Swap the screen buffers
        glfwSwapBuffers(window);
    }

    glfwTerminate();
    return 0;
}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    if(action == GLFW_PRESS)
        keys[key] = true;
    else if(action == GLFW_RELEASE)
        keys[key] = false;
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    if(key == GLFW_KEY_G && action == GLFW_PRESS)
        GPU = !GPU;

}
void do_movement()
{
    GLfloat cameraSpeed = 5.0f * deltaTime;
    if(keys[GLFW_KEY_W])
        cameraPos += cameraSpeed * cameraFront;
    if(keys[GLFW_KEY_S])
        cameraPos -= cameraSpeed * cameraFront;
    if(keys[GLFW_KEY_A])
        cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    if(keys[GLFW_KEY_D])
        cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;


}

bool firstMouse = true;

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;
    lastX = xpos;
    lastY = ypos;

    GLfloat sensitivity = 0.05;
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw   += xoffset;
    pitch += yoffset;

    if(pitch > 89.0f)
        pitch = 89.0f;
    if(pitch < -89.0f)
        pitch = -89.0f;

    glm::vec3 front;
    front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    front.y = sin(glm::radians(pitch));
    front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    cameraFront = glm::normalize(front);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    if(fov >= 1.0f && fov <= 45.0f)
        fov -= yoffset/10;
    if(fov <= 1.0f)
        fov = 1.0f;
    if(fov >= 45.0f)
        fov = 45.0f;
}
int getError()
{
    GLenum err (glGetError());
    while(err!=GL_NO_ERROR && err!=GL_INVALID_ENUM) {
        string error;

        switch(err) {
            case GL_INVALID_OPERATION:      error="INVALID_OPERATION";      break;
            case GL_INVALID_VALUE:          error="INVALID_VALUE";          break;
            case GL_OUT_OF_MEMORY:          error="OUT_OF_MEMORY";          break;
            case GL_INVALID_FRAMEBUFFER_OPERATION:  error="INVALID_FRAMEBUFFER_OPERATION";  break;
        }

        cerr << "GL_" << error.c_str() << endl;
        err=glGetError();
        return 1;
    }
    return 0;
}
void makeLight(Shader ourShader)
{
    glUniform3f(glGetUniformLocation(ourShader.Program, "dirLight.direction"), -1.0f, -1.0f, 1.0f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "dirLight.ambient"), 0.5f, 0.5f, 0.5f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "dirLight.diffuse"), 0.1f, 0.1f, 0.1f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "dirLight.specular"), 0.1f, 0.1f, 0.1f);

    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[0].position"), 2.0f, 2.0f, 2.0f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[0].ambient"), 0.05f, 0.05f, 0.05f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[0].diffuse"), 0.8f, 0.8f, 0.8f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[0].specular"), 0.2f, 1.2f, 1.2f);
    glUniform1f(glGetUniformLocation(ourShader.Program, "pointLights[0].constant"), 1.0f);
    glUniform1f(glGetUniformLocation(ourShader.Program, "pointLights[0].linear"), 0.09);
    glUniform1f(glGetUniformLocation(ourShader.Program, "pointLights[0].quadratic"), 0.032);

    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[1].position"), -2.0f, 2.0f, 2.0f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[1].ambient"), 0.05f, 0.05f, 0.05f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[1].diffuse"), 0.8f, 0.8f, 0.8f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[1].specular"), 1.2f, 1.2f, 0.2f);
    glUniform1f(glGetUniformLocation(ourShader.Program, "pointLights[1].constant"), 1.0f);
    glUniform1f(glGetUniformLocation(ourShader.Program, "pointLights[1].linear"), 0.09);
    glUniform1f(glGetUniformLocation(ourShader.Program, "pointLights[1].quadratic"), 0.032);

    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[2].position"), 2.0f, 2.0f, -2.0f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[2].ambient"), 0.05f, 0.05f, 0.05f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[2].diffuse"), 0.8f, 0.8f, 0.8f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[2].specular"), 1.0f, 1.0f, 1.0f);
    glUniform1f(glGetUniformLocation(ourShader.Program, "pointLights[2].constant"), 1.0f);
    glUniform1f(glGetUniformLocation(ourShader.Program, "pointLights[2].linear"), 0.09);
    glUniform1f(glGetUniformLocation(ourShader.Program, "pointLights[2].quadratic"), 0.032);

    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[3].position"), -2.0f, 2.0f, -2.0f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[3].ambient"), 0.05f, 0.05f, 0.05f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[3].diffuse"), 0.8f, 0.8f, 0.8f);
    glUniform3f(glGetUniformLocation(ourShader.Program, "pointLights[3].specular"), 0.2f, 0.2f, 0.2f);
    glUniform1f(glGetUniformLocation(ourShader.Program, "pointLights[3].constant"), 1.0f);
    glUniform1f(glGetUniformLocation(ourShader.Program, "pointLights[3].linear"), 0.09);
    glUniform1f(glGetUniformLocation(ourShader.Program, "pointLights[3].quadratic"), 0.032);

    glUniform1f(glGetUniformLocation(ourShader.Program, "material.shininess"), 64.0f);
    glUniform1f(glGetUniformLocation(ourShader.Program, "material.diffuse"), 1.0f);
    glUniform1f(glGetUniformLocation(ourShader.Program, "material.specular"), 0.4f);
}

void collision(Mesh &mesh, GLfloat floorLevel)
{
    if (mesh.getPosition().y - ((float)mesh.getSize()/20) <= floorLevel && length(mesh.getSpeed()) > 0.0000001f )
    {
//        mesh.tickBlock(glm::vec3(0,0.1,0));

//        mesh.setSpeed(glm::vec3(0,0,0));
//        mesh.useForce(glm::vec3(0,4,0));

        if(mesh.getSpeed().y < 0) {
            mesh.useForce(-mesh.getSpeed() * 2.0f);
            mesh.tickBlock(glm::vec3(0,0.1,0));
        }
        else if(abs(mesh.getSpeed().y) <= 0.00001)
        {
            mesh.useForce(glm::vec3(0,0.01,0));
        }
    }
}