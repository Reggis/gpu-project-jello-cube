//
// Created by reggis on 13.09.15.
//

#ifndef OPENGLSIMPLE_VERTEX_H
#define OPENGLSIMPLE_VERTEX_H

#include <glm/glm.hpp>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include <host_defines.h>
#include <thrust/host_vector.h>

using namespace std;
class Vertex
{
public:
    glm::vec3 position;
    float mass;
    glm::vec3 speed;
    glm::vec3 acceleration;
    int number;
    void __device__ __host__ useForce(glm::vec3 force, float dt);
    glm::vec3 __device__ __host__ getSpeed();
    void __device__ __host__ makeMove();
};

#endif //OPENGLSIMPLE_VERTEX_H
