//
// Created by reggis on 08.09.15.
//

#include "Mesh.h"
#include "calculations.h"

Mesh::Mesh(int size) {
    this->size = size;
    this->spaceLength = 0.1;
    this->hook =0.1;
    this->dumping=0.01;
    this->dt=0.1f;
    int start = -floor(size/2);
    int stop = floor(size/2);
    this->translation = glm::vec3(0,0,0);
    if(size%2==1)
        stop++;
    for(int i=start; i<stop; i++)
    {
        vector<vector<Vertex>> plane;
        for(int j=start; j<stop; j++)
        {
            vector<Vertex> line;
            for(int k=start; k<stop; k++)
            {
                Vertex vert;
                vert.position.x=i*this->spaceLength;
                vert.position.y=j*this->spaceLength;
                vert.position.z=k*this->spaceLength;
                vert.mass =1;
                vert.number =-1;
                vert.speed=glm::vec3(0,0,0);
                vert.acceleration=glm::vec3(0,0,0);
                line.push_back(vert);
            }
            plane.push_back(line);
        }
        this->points.push_back(plane);
    }
    this->generateFaces();
}

void Mesh::generateFaces() {
    cout << "Face generation" << endl;
    //Sciana przednia
    // Z=0
    cout << "Front" << endl;
    for(int i=0; i<this->size-1; i++)
    {
        for(int j=0; j<this->size-1; j++)
        {
            this->saveFacesAndVertex(this->points[i][j][0],
                                     this->points[i+1][j][0],
                                     this->points[i][j+1][0],
                                     this->points[i+1][j+1][0]
            );
        }
    }
//
//    //Sciana tylna
//    // Z=size-1
//
    cout << "Back" << endl;
    for(int i=0; i<this->size-1; i++)
    {
        for(int j=0; j<this->size-1; j++)
        {
            this->saveFacesAndVertex(
                this->points[i+1][j][this->size-1],
                this->points[i][j][this->size-1],
                this->points[i+1][j+1][this->size-1],
                this->points[i][j+1][this->size-1]

            );
        }
    }
//
//    //Sciana lewa
//    // X=0
//
    cout << "Left" << endl;
    for(int i=0; i<this->size-1; i++)
    {
        for(int j=0; j<this->size-1; j++)
        {
            this->saveFacesAndVertex(
                    this->points[0][i][j],
                    this->points[0][i+1][j],
                    this->points[0][i][j+1],
                    this->points[0][i+1][j+1]
            );
        }
    }

//    //Sciana prawa
//    //X=size-1
    cout << "Right" << endl;
    for(int i=0; i<this->size-1; i++)
    {
        for(int j=0; j<this->size-1; j++)
        {
            this->saveFacesAndVertex(
                    this->points[this->size-1][i+1][j],
                    this->points[this->size-1][i][j],
                    this->points[this->size-1][i+1][j+1],
                this->points[this->size-1][i][j+1]

            );
        }
    }
//    //Sciana dolna
//    // Y=0
    cout << "Bottom" << endl;
    for(int i=0; i<this->size-1; i++)
    {
        for(int j=0; j<this->size-1; j++)
        {
            this->saveFacesAndVertex(
                    this->points[i+1][0][j],
                this->points[i][0][j],
                    this->points[i+1][0][j+1],
                this->points[i][0][j+1]

            );

        }
    }

//    //Sciana gorna
//    // Y=size-1
    cout << "Topw" << endl;
    for(int i=0; i<this->size-1; i++)
    {
        for(int j=0; j<this->size-1; j++)
        {
            this->saveFacesAndVertex(this->points[i][this->size-1][j],
                                     this->points[i+1][this->size-1][j],
                                     this->points[i][this->size-1][j+1],
                                     this->points[i+1][this->size-1][j+1]
            );
        }
    }
    cout << "Face generation end" << endl;

}

void Mesh::saveFacesAndVertex(Vertex &p1, Vertex &p2, Vertex &p3, Vertex &p4)
{
    if(p1.number == -1)
    {
        p1.number = this->drawedPoints.size();
        DrawedPoint point;
        point.position = p1.position;
        drawedPoints.push_back(point);
    }
    if(p2.number == -1)
    {
        p2.number = this->drawedPoints.size();
        DrawedPoint point;
        point.position = p2.position;
        drawedPoints.push_back(point);
    }
    if(p3.number == -1)
    {
        p3.number = this->drawedPoints.size();
        DrawedPoint point;
        point.position = p3.position;
        drawedPoints.push_back(point);
    }
    if(p4.number == -1)
    {
        p4.number = this->drawedPoints.size();
        DrawedPoint point;
        point.position = p4.position;
        drawedPoints.push_back(point);
    }


    Face face;
    face.a=p1.number;
    face.b=p2.number;
    face.c=p3.number;

    faces.push_back(face);

    Face face2;
    face2.a=p2.number;
    face2.b=p4.number;
    face2.c=p3.number;

    faces.push_back(face2);


    drawedPoints[p1.number].faces.push_back(this->faces.size()-2);

    drawedPoints[p2.number].faces.push_back(this->faces.size()-2);
    drawedPoints[p2.number].faces.push_back(this->faces.size()-1);


    drawedPoints[p3.number].faces.push_back(this->faces.size()-2);
    drawedPoints[p3.number].faces.push_back(this->faces.size()-1);

    drawedPoints[p4.number].faces.push_back(this->faces.size()-1);

    this->generateNormals();
}

vector<float> Mesh::getVerticles() {
    vector<float> v;
    for(int i=0; i<this->drawedPoints.size(); i++)
    {
//        cout << drawedPoints[i].position.x << ", " << drawedPoints[i].position.y << ", " << drawedPoints[i].position.z << ", " << drawedPoints[i].normal.x << "," << drawedPoints[i].normal.y << ","  << drawedPoints[i].normal.z << "," << endl;
        v.push_back(drawedPoints[i].position.x);
        v.push_back(drawedPoints[i].position.y);
        v.push_back(drawedPoints[i].position.z);
        v.push_back(drawedPoints[i].normal.x);
        v.push_back(drawedPoints[i].normal.y);
        v.push_back(drawedPoints[i].normal.z);
        v.push_back(1.0);
        v.push_back(0.2);
        v.push_back(0.2);
    }
    return v;
}

vector<GLuint> Mesh::getFaces() {
    vector<GLuint> v;
    for(int i=0; i<this->faces.size(); i++)
    {
//        cout << faces[i].a << ", " << faces[i].b << ", " << faces[i].c << ","  << faces[i].normal.x << "," << faces[i].normal.y << ","  << faces[i].normal.z << "," << endl;
        v.push_back(faces[i].a);
        v.push_back(faces[i].b);
        v.push_back(faces[i].c);
    }
    return v;
}

void Mesh::generateNormals()
{
    for(int i=0; i<this->faces.size(); i++)
    {
        faces[i].normal=normalFromVectors(drawedPoints[faces[i].a].position, drawedPoints[faces[i].b].position, drawedPoints[faces[i].c].position);
    }

    for(int i=0; i<this->drawedPoints.size(); i++) {
        glm::vec3 N(0,0,0);
        for(int j=0; j<drawedPoints[i].faces.size(); j++)
        {
            N+= faces[drawedPoints[i].faces[j]].normal;
        }
        this->drawedPoints[i].normal = normalize(N);
    }
}

void Mesh::updateNormals()
{
    for(int i=0; i<this->faces.size(); i++)
    {
        glm::vec3 new_normal;
        new_normal = normalFromVectors(drawedPoints[faces[i].a].position, drawedPoints[faces[i].b].position, drawedPoints[faces[i].c].position);
        if(!isnan(length(new_normal -  faces[i].normal)))
        faces[i].normal= new_normal;
//        else
//            cout << length(new_normal -  faces[i].normal) <<endl;
    }

//    cout << "GENERATE VERTEX NORMALS" <<endl;
    for(int i=0; i<this->drawedPoints.size(); i++) {
        glm::vec3 N(0,0,0);
//        cout << "VERTEX NO " << i <<endl;
        for(int j=0; j<drawedPoints[i].faces.size(); j++)
        {
//            cout << "Adding to normal " << faces[drawedPoints[i].faces[j]].normal.x << " " << faces[drawedPoints[i].faces[j]].normal.y << " " << faces[drawedPoints[i].faces[j]].normal.z << endl;
            N+= faces[drawedPoints[i].faces[j]].normal;
        }
        this->drawedPoints[i].normal = normalize(N);
    }
}

glm::vec3 Mesh::normalFromVectors(glm::vec3 A, glm::vec3 B, glm::vec3 C){
    glm::vec3 N = cross(A-B,C-B);
    float sin_alpha = length(N) / (length(B-A) * length(C-A) );
    return normalize(N) * asin(sin_alpha);
    return normalize(N);
}

void Mesh::useForce(glm::vec3 force)
{
    this->movementSpeed += force;
    if(abs(this->movementSpeed.x) < 0.000001)
        this->movementSpeed.x=0;
    if(abs(this->movementSpeed.y) < 0.000001)
        this->movementSpeed.y=0;
    if(abs(this->movementSpeed.z) < 0.000001)
        this->movementSpeed.z=0;
}

void Mesh::move()
{
    this->translation += this->movementSpeed;
}

glm::vec3 Mesh::getPosition() {
    return this->translation;
}

glm::vec3 Mesh::getSpeed()
{
    return this->movementSpeed;
}

int Mesh::getSize()
{
    return this->size;
}



void Mesh::computeForces(bool gpu, float speed)
{
    this->dt*=speed;

    if(gpu)
    {
        glm::vec3 *forces = computeForceOnGPU(this->size, this->points);
        int size = this->size;
        int size_mult = size*size;
        for (int i = 0; i < this->size; i++) {
//        cout << "block " << i <<endl;
            for (int j = 0; j < this->size; j++) {
//            cout << "column " << j <<endl;
                for (int k = 0; k < this->size; k++) {
                    this->points[i][j][k].useForce(forces[i*size_mult+j*size+k], this->dt);
                }
            }
        }

    }
    else {
        glm::vec3 allforces(0, 0, 0);
//    cout <<"Computing forces" << endl;
        for (int i = 0; i < this->size; i++) { // Myk - jeden rzadek sie nei rusza
//        cout << "block " << i <<endl;
            for (int j = 0; j < this->size; j++) {
//            cout << "column " << j <<endl;
                for (int k = 0; k < this->size; k++) {
//                cout << "row " << k <<endl;

                    glm::vec3 force(0, 0, 0);
                    float multiplicator = 1;

                    //Structural
                    // i-1,j,k
                    if (i - 1 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i - 1][j][k], multiplicator);
                    }
                    // i+1,j,k
                    if (i + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i + 1][j][k], multiplicator);
                    }
                    // i,j-1,k
                    if (j - 1 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i][j - 1][k], multiplicator);
                    }
                    // i,j+1,k
                    if (j + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i][j + 1][k], multiplicator);
                    }
                    // i,j,k-1
                    if (k - 1 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i][j][k - 1], multiplicator);
                    }
                    // i,j,k+1
                    if (k + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i][j][k + 1], multiplicator);
                    }

                    multiplicator = sqrt(2);

//                // i,j-1,k-1
//
                    if (j - 1 >= 0 && k - 1 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i][j - 1][k - 1], multiplicator);
                    }

                    // i,j-1,k+1

                    if (j - 1 >= 0 && k + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i][j - 1][k + 1], multiplicator);
                    }

                    // i,j+1,k-1

                    if (k - 1 >= 0 && j + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i][j + 1][k - 1], multiplicator);
                    }

                    // i,j+1,k+1

                    if (j + 1 < this->size && k + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i][j + 1][k + 1], multiplicator);
                    }

                    ////
                    // i-1,j,k-1

                    if (i - 1 >= 0 && k - 1 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i - 1][j][k - 1], multiplicator);
                    }

                    // i-1,j,k+1

                    if (i - 1 >= 0 && k + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i - 1][j][k + 1], multiplicator);
                    }

                    // i-1,j-1,k

                    if (i - 1 >= 0 && j - 1 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i - 1][j - 1][k], multiplicator);
                    }


                    // i-1,j-1,k-1
                    multiplicator = sqrt(3);
                    if (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i - 1][j - 1][k - 1], multiplicator);
                    }

                    // i-1,j-1,k+1

                    if (i - 1 >= 0 && j - 1 >= 0 && k + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i - 1][j - 1][k + 1], multiplicator);
                    }

                    // i-1,j+1,k
                    multiplicator = sqrt(2);
                    if (i - 1 >= 0 && j + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i - 1][j + 1][k], multiplicator);
                    }

                    // i-1,j+1,k-1
                    multiplicator = sqrt(3);
                    if (i - 1 >= 0 && j + 1 < this->size && k - 1 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i - 1][j + 1][k - 1], multiplicator);
                    }

                    // i-1,j+1,k+1

                    if (i - 1 >= 0 && j + 1 < this->size && k + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i - 1][j + 1][k + 1], multiplicator);
                    }

                    ///
                    multiplicator = sqrt(2);
                    // i+1,j,k-1
                    if (i + 1 < this->size && k - 1 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i + 1][j][k - 1], multiplicator);
                    }

                    // i+1,j,k+1
                    if (i + 1 < this->size && k + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i + 1][j][k + 1], multiplicator);
                    }

                    // i+1,j-1,k
                    if (i + 1 < this->size && j - 1 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i + 1][j - 1][k], multiplicator);
                    }

                    // i+1,j-1,k-1
                    multiplicator = sqrt(3);
                    if (i + 1 < this->size && j - 1 >= 0 && k - 1 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i + 1][j - 1][k - 1], multiplicator);
                    }

                    // i+1,j-1,k+1

                    if (i + 1 < this->size && j - 1 >= 0 && k + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i + 1][j - 1][k + 1], multiplicator);
                    }

                    // i+1,j+1,k
                    multiplicator = sqrt(2);
                    if (i + 1 < this->size && j + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i + 1][j + 1][k], multiplicator);
                    }

                    // i+1,j+1,k-1
                    multiplicator = sqrt(3);
                    if (i + 1 < this->size && j + 1 < this->size && k - 1 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i + 1][j + 1][k - 1], multiplicator);
                    }

                    // i+1,j+1,k+1

                    if (i + 1 < this->size && j + 1 < this->size && k + 1 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i + 1][j + 1][k + 1], multiplicator);
                    }


                    multiplicator = 2;
                    //Bend
                    // i-2,j,k
//                cout << i-2 << endl;
                    if (i - 2 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i - 2][j][k], multiplicator);
                    }

                    // i+2,j,k
                    if (i + 2 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i + 2][j][k], multiplicator);
                    }
                    // i,j-2,k
                    if (j - 2 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i][j - 2][k], multiplicator);
                    }
                    // i,j+2,k
                    if (j + 2 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i][j + 2][k], multiplicator);
                    }
                    // i,j,k-2
                    if (k - 2 >= 0) {
                        force += calcPair(this->points[i][j][k], this->points[i][j][k - 2], multiplicator);
                    }
                    // i,j,k+2
                    if (k + 2 < this->size) {
                        force += calcPair(this->points[i][j][k], this->points[i][j][k + 2], multiplicator);
                    }
                    this->points[i][j][k].useForce(force, this->dt);
                    allforces += force;
                }
            }
        }
    }
}
glm::vec3 Mesh::calcPair(Vertex &current, Vertex &neighbour, float multiplicator)
{
    glm::vec3 space = (current.position - neighbour.position);
    glm::vec3 hook = -this->hook * (length(space) - this->spaceLength*multiplicator) * normalize(space); // Hook law
    glm::vec3 dumping = -this->dumping * dot((current.getSpeed() - neighbour.getSpeed()),space)/length(space) *  normalize(space); //damping force
    glm::vec3 returnForce = hook + dumping;
    return returnForce;
}

void Mesh::makeMove()
{
    for(int i=0; i<this->size; i++) { // Myk - jeden rzadek sie nei rusza
        for (int j = 0; j < this->size; j++) {
            for (int k = 0; k < this->size; k++) {
                this->points[i][j][k].makeMove();

            }
        }
    }
    this->generateOutputData();
}
void Mesh::generateOutputData()
{
    for(int i=0; i<this->size; i++) { //
        for (int j = 0; j < this->size; j++) {
            for (int k = 0; k < this->size; k++) {
                int number = this->points[i][j][k].number;
                if(number != -1)
                {
                    if(this->drawedPoints[number].position != this->points[i][j][k].position)
                    this->drawedPoints[number].position = this->points[i][j][k].position;
                }
            }
        }
    }
    this->updateNormals();
}
void Mesh::tickBlock(glm::vec3 force)
{
    for (int j = 0; j < this->size; j++) {
        for (int k = 0; k < this->size; k++) {
            this->points[1][j][k].useForce(force, this->dt);
        }
    }
}

void Mesh::setSpeed(glm::vec3 speed)
{
    this->movementSpeed = speed;
}
vector<vector<vector<Vertex>>> Mesh::getPoints()
{
    return this->points;
}
