//
// Created by reggis on 13.09.15.
//

#ifndef OPENGLSIMPLE_CALCULATIONS_H_H
#define OPENGLSIMPLE_CALCULATIONS_H_H

#include <vector>
#include "Vertex.h"

glm::vec3* computeForceOnGPU(int size, vector<vector<vector<Vertex>>> points);
int some_function(int num, vector<vector<vector<Vertex>>> points);

#endif //OPENGLSIMPLE_CALCULATIONS_H_H
