#include <stdio.h>
#include <cuda_runtime.h>
#include <curand_mtgp32_kernel.h>
#include <device_launch_parameters.h>
#include "calculations.h"
#include "Vertex.h"

__device__ glm::vec3 calcPair(Vertex &current, Vertex &neighbour, float multiplicator)
{
    glm::vec3 space = (current.position - neighbour.position);
    glm::vec3 hook =-0.1f * (length(space) - 0.1f*multiplicator) * normalize(space); // Hook law
    glm::vec3 dumping = -0.05f * dot((current.speed - neighbour.speed),space)/length(space) *  normalize(space); //damping force
    glm::vec3 returnForce = hook+dumping;
    return returnForce;
}

__global__ void sumForce(Vertex* A, glm::vec3* C, int N, int M)
{
    int tId = threadIdx.x + blockIdx.x * blockDim.x;

    while(tId <N)
    {
        int i = tId/(M*M);
        int j = (tId%(M*M))/M;
        int k = (tId%(M*M))%M;

        glm::vec3 force(0, 0, 0);
        float multiplicator = 1;

        // Calculation all forces - structural, shear and bend

        //Structural
        // i-1,j,k
        if (i - 1 >= 0) {
            force += calcPair(A[tId], A[(i - 1)*M*M + j*M +k], multiplicator);
        }
        // i+1,j,k
        if (i + 1 < M) {
            force += calcPair(A[tId], A[(i + 1)*M*M + j*M +k], multiplicator);
        }
        // i,j-1,k
        if (j - 1 >= 0) {
            force += calcPair(A[tId], A[(i)*M*M + (j-1)*M +(k)], multiplicator);
        }
        // i,j+1,k
        if (j + 1 < M) {
            force += calcPair(A[tId], A[(i)*M*M + (j+1)*M +(k)], multiplicator);
        }
        // i,j,k-1
        if (k - 1 >= 0) {
            force += calcPair(A[tId], A[(i)*M*M + (j)*M +(k-1)], multiplicator);
        }
        // i,j,k+1
        if (k + 1 < M) {
            force += calcPair(A[tId], A[(i)*M*M + (j)*M +(k+1)], multiplicator);
        }

        multiplicator = sqrt(2.0f);

        // i,j-1,k-1

        if (j - 1 >= 0 && k - 1 >= 0) {
            force += calcPair(A[tId], A[i*M*M + (j - 1)*M + k - 1], multiplicator);
        }

        // i,j-1,k+1

        if (j - 1 >= 0 && k + 1 < M) {
            force += calcPair(A[tId], A[i*M*M + (j - 1)*M +k + 1], multiplicator);
        }

        // i,j+1,k-1

        if (k - 1 >= 0 && j + 1 < M) {
            force += calcPair(A[tId], A[i*M*M +(j + 1)*M +k - 1], multiplicator);
        }

        // i,j+1,k+1

        if (j + 1 < M && k + 1 < M) {
            force += calcPair(A[tId], A[i*M*M + (j + 1)*M +k + 1], multiplicator);
        }

        ////
        // i-1,j,k-1

        if (i - 1 >= 0 && k - 1 >= 0) {
            force += calcPair(A[tId], A[(i - 1)*M*M +j*M + k - 1], multiplicator);
        }

        // i-1,j,k+1

        if (i - 1 >= 0 && k + 1 < M) {
            force += calcPair(A[tId], A[(i - 1)*M*M + j*M +k + 1], multiplicator);
        }

        // i-1,j-1,k

        if (i - 1 >= 0 && j - 1 >= 0) {
            force += calcPair(A[tId], A[(i - 1)*M*M +(j - 1)*M +k], multiplicator);
        }


        // i-1,j-1,k-1
        multiplicator = sqrt(3.0f);
        if (i - 1 >= 0 && j - 1 >= 0 && k - 1 >= 0) {
            force += calcPair(A[tId], A[(i - 1)*M*M + (j - 1)*M +k - 1], multiplicator);
        }

        // i-1,j-1,k+1

        if (i - 1 >= 0 && j - 1 >= 0 && k + 1 < M) {
            force += calcPair(A[tId], A[(i - 1)*M*M +(j - 1)*M +k + 1], multiplicator);
        }

        // i-1,j+1,k
        multiplicator = sqrt(2.0f);
        if (i - 1 >= 0 && j + 1 < M) {
            force += calcPair(A[tId], A[(i - 1)*M*M +(j + 1)*M +k], multiplicator);
        }

        // i-1,j+1,k-1
        multiplicator = sqrt(3.0f);
        if (i - 1 >= 0 && j + 1 < M && k - 1 >= 0) {
            force += calcPair(A[tId], A[(i - 1)*M*M+ (j + 1)*M +k - 1], multiplicator);
        }

        // i-1,j+1,k+1

        if (i - 1 >= 0 && j + 1 < M && k + 1 < M) {
            force += calcPair(A[tId], A[(i - 1)*M*M +(j + 1)*M +k + 1], multiplicator);
        }

        ///
        multiplicator = sqrt(2.0f);
        // i+1,j,k-1
        if (i + 1 < M && k - 1 >= 0) {
            force += calcPair(A[tId], A[(i + 1)*M*M +j*M +k - 1], multiplicator);
        }

        // i+1,j,k+1
        if (i + 1 < M && k + 1 < M) {
            force += calcPair(A[tId], A[(i + 1)*M*M +j*M +k + 1], multiplicator);
        }

        // i+1,j-1,k
        if (i + 1 < M && j - 1 >= 0) {
            force += calcPair(A[tId], A[(i + 1)*M*M + (j - 1)*M +k], multiplicator);
        }

        // i+1,j-1,k-1
        multiplicator = sqrt(3.0f);
        if (i + 1 < M && j - 1 >= 0 && k - 1 >= 0) {
            force += calcPair(A[tId], A[(i + 1)*M*M + (j - 1)*M +k - 1], multiplicator);
        }

        // i+1,j-1,k+1

        if (i + 1 < M && j - 1 >= 0 && k + 1 < M) {
            force += calcPair(A[tId], A[(i + 1)*M*M + (j - 1)*M + k + 1], multiplicator);
        }

        // i+1,j+1,k
        multiplicator = sqrt(2.0f);
        if (i + 1 < M && j + 1 < M) {
            force += calcPair(A[tId], A[(i + 1)*M*M + (j + 1)*M + k], multiplicator);
        }

        // i+1,j+1,k-1
        multiplicator = sqrt(3.0f);
        if (i + 1 < M && j + 1 < M && k - 1 >= 0) {
            force += calcPair(A[tId], A[(i + 1)*M*M + (j + 1)*M + k - 1], multiplicator);
        }

        // i+1,j+1,k+1

        if (i + 1 < M && j + 1 < M && k + 1 < M) {
            force += calcPair(A[tId], A[(i + 1)*M*M + (j + 1)*M + k + 1], multiplicator);
        }


        multiplicator = 2;
        //Bend
        // i-2,j,k
//                cout << i-2 << endl;
        if (i - 2 >= 0) {
            force += calcPair(A[tId], A[(i - 2)*M*M +j*M +k], multiplicator);
        }

        // i+2,j,k
        if (i + 2 < M) {
            force += calcPair(A[tId], A[(i + 2)*M*M + j*M +k], multiplicator);
        }
        // i,j-2,k
        if (j - 2 >= 0) {
            force += calcPair(A[tId], A[i*M*M +(j - 2)*M +k], multiplicator);
        }
        // i,j+2,k
        if (j + 2 < M) {
            force += calcPair(A[tId], A[i*M*M +(j + 2)*M +k], multiplicator);
        }
        // i,j,k-2
        if (k - 2 >= 0) {
            force += calcPair(A[tId], A[i*M*M + j*M +k - 2], multiplicator);
        }
        // i,j,k+2
        if (k + 2 < M) {
            force += calcPair(A[tId], A[i*M*M +j*M+k + 2], multiplicator);
        }

        //Calculate forces
        C[tId] = force;



        tId += blockDim.x * gridDim.x;
    }

}




glm::vec3* computeForceOnGPU(int num, vector<vector<vector<Vertex>>> points)
{
    int tPB = 32; // ilosc watkow w bloku. Musi byc potega 2

    //Dlugosc wektorow wejsciowych
    int N = num*num*num;

    //Rozmiar przesylanych danych
    size_t size = N*sizeof(Vertex);

    Vertex* h_A = (Vertex*)malloc(size);

    glm::vec3* h_C = (glm::vec3*)malloc(N*sizeof(glm::vec3));

    for(int i=0; (i < num); i++)
    {
        for(unsigned j=0; (j < num); j++)
        {
            for(unsigned k=0; (k < num); k++)
            {
                h_A[i*num*num + j*num + k] = points[i][j][k];
            }
        }
    }
    //Alokowanie pamieci dla urzadzenia
    Vertex* d_A;
    cudaMalloc(&d_A, size);

    glm::vec3* d_C;
    cudaMalloc(&d_C, N*sizeof(glm::vec3));

    cudaMemcpy(d_A, h_A, size, cudaMemcpyHostToDevice);

    int blocksPerGrid = (N+tPB -1)/tPB;
    size_t memory = tPB*sizeof(Vertex);
    sumForce<<<blocksPerGrid, tPB, memory>>>(d_A, d_C, N, num);

    cudaMemcpy(h_C, d_C, N*sizeof(glm::vec3), cudaMemcpyDeviceToHost);

    cudaFree(d_A);
    cudaFree(d_C);

//    printf("Result \n");
//    for(int i=0; i<N; i++)
//        printf("%f, %f, %f\n", h_C[i].x, h_C[i].y, h_C[i].z);

    free(h_A);
    return h_C;
}
