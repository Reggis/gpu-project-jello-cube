//
// Created by reggis on 08.09.15.
//

#ifndef OPENGLSIMPLE_MESH_H
#define OPENGLSIMPLE_MESH_H

#include <glm/glm.hpp>
#include <vector>
#include <iostream>
#include <GL/glew.h>
#include "Vertex.h"


using namespace std;

struct DrawedPoint
{
    glm::vec3 position;
    glm::vec3 normal;
    vector<int> faces;
};
struct Face
{
    GLuint a;
    GLuint b;
    GLuint c;
    glm::vec3 normal;
};


using namespace std;

class Mesh {
public:
    Mesh(int size);
    vector<float> getVerticles();
    vector<GLuint> getFaces();
    glm::vec3 getPosition();
    void useForce(glm::vec3 force);
    void move();
    glm::vec3 getSpeed();
    int getSize();
    void makeMove();
    void computeForces(bool gpu, float speed);
    void tickBlock(glm::vec3 force);
    void setSpeed(glm::vec3 speed);
    void updateNormals();
    vector<vector<vector<Vertex>>> getPoints();

private:
    void generateOutputData();
    glm::vec3 calcPair(Vertex &current, Vertex &neighbour, float multiplicator);
    void generateFaces();
    void saveFacesAndVertex(Vertex &p1, Vertex &p2, Vertex &p3, Vertex &p4);
    void generateNormals();
    glm::vec3 normalFromVectors(glm::vec3 A, glm::vec3 B, glm::vec3 C);
    int size;
    float hook;
    float dumping;
    float spaceLength;
    float dt;
    vector<Face> faces;
    vector<DrawedPoint> drawedPoints;
    vector<vector<vector<Vertex>>> points; // X, Y, Z
    glm::vec3 movementSpeed;
    glm::vec3 translation;
};


#endif //OPENGLSIMPLE_MESH_H
